package koji;

public class Fireball {
	
	private int x, y, speedX;
	private boolean visible;

	public Fireball(int startX, int startY) {
		
		x = startX;
		y = startY;
		speedX = 4;
		visible = true;
		
	}
	
	public void update(){
		
		//Continually updates the fireballs in the x direction (left to right)
		x += speedX;
		//Deletes an individual Fireball when it goes beyond the 900px background
		if (x > 600){
			visible = false;
		}
		
	}

	
	// A variety of getters and setters
	
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

}

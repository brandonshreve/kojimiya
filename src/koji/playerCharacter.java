package koji;

import java.util.ArrayList;

public class playerCharacter {
	
	private int jumpHeight = -20;
	private int walkSpeed = 5;
    private int originX = 100;
	private int groundLevel = 355;
    private int originY = groundLevel;

	private boolean jumped = false;
	private boolean movingLeft = false;
	private boolean movingRight = false;
	private boolean ducked = false;
	private boolean superCharacter = false;

	//Setting initial speeds in the X and Y direction.
	private int speedX = 0;
	private int speedY = 0;
	
    private int jumpCount = 0; 
    private int fireballCount = 0; 
	
	private Background bg1 = StartGame.getBg1();
	private Background bg2 = StartGame.getBg2();
	
	// The array of fireballs. Will be created on Control presses
	private ArrayList<Fireball> fireballs = new ArrayList<Fireball>();
	
	public void update() {
		// Moves Character or Scrolls Background accordingly.

		if (speedX < 0) {
			originX += speedX;
		}
		if (speedX == 0 || speedX < 0) {
			bg1.setSpeedX(0);
			bg2.setSpeedX(0);

		}
		if (originX <= 200 && speedX > 0) {
			originX += speedX;
		}
		
		if (speedX > 0 && originX > 200){
			bg1.setSpeedX(-walkSpeed);
			bg2.setSpeedX(-walkSpeed);
		}

		// Updates Y Position
		originY += speedY;
		if (originY + speedY >= groundLevel) {
			originY = groundLevel;
		}

		// Getting the Y-coordinate of the player to increase by 1 while in a jump
		if (jumped == true) {
			speedY += 1;

			if (originY + speedY >= groundLevel) {
				originY = groundLevel;
				speedY = 0;
				jumped = false;
			}

		}

		// Allows Player to go slightly offscreen but no further.
		if (originX + speedX <= 40) {
			originX = 41;
		}
	}

	//Allows the player to moveRight if they are not ducked. walkSpeed is a static variable.
	public void moveRight() {
		if (ducked == false) {
			speedX = walkSpeed;
		}
	}

	//Allows the player to moveLeft if they are not ducked. walkSpeed is a static variable.
	public void moveLeft() {
		if (ducked == false) {
			speedX = -walkSpeed;
		}
	}

	public void stopRight() {
		setMovingRight(false);
		stop();
	}

	public void stopLeft() {
		setMovingLeft(false);
		stop();
	}

	private void stop() {
		if (isMovingRight() == false && isMovingLeft() == false) {
			speedX = 0;
		}

		if (isMovingRight() == false && isMovingLeft() == true) {
			moveLeft();
		}

		if (isMovingRight() == true && isMovingLeft() == false) {
			moveRight();
		}

	}

	public void jump() {
		if (jumped == false) {
			speedY = jumpHeight;
			jumped = true;
		}

	}
	
	public void shoot(){
		//this fireball object is ONLY aligned for Mario, not super Mario.
		//Only Super Mario can use fireball. this is only for testing
		Fireball f = new Fireball((originX - 30), (originY - 45));
		fireballs.add(f);
	}

	
    //These two Methods keep track of number of Jumps and the  
    public void jumpCounter(){ 
        jumpCount += 1; 
          
        if(jumpCount == 1){ 
            System.out.println("Player PLAYERNAME jumped " + jumpCount + " time. HIGH SCORE: NOT IMPLEMENTED"); 
        } 
        else { 
            System.out.println("Player PLAYERNAME jumped " + jumpCount + " times. HIGH SCORE: NOT IMPLEMENTED"); 
        } 
              
    } 
      
      
    public void fireballCounter(){ 
        fireballCount += 1; 
          
        if(fireballCount == 1){ 
            System.out.println("Player PLAYERNAME shot " + fireballCount + " time. HIGH SCORE: NOT IMPLEMENTED"); 
        } 
        else { 
            System.out.println("Player PLAYERNAME shot " + fireballCount + " times. HIGH SCORE: NOT IMPLEMENTED"); 
        } 
              
    }
	
    
	//Variety of getters and setters
	public int getOriginX() {
		return originX;
	}

	public int getOriginY() {
		return originY;
	}

	public boolean isJumped() {
		return jumped;
	}
	
	public boolean isSuper(){
		return superCharacter;
	}

	public int getSpeedX() {
		return speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setOriginX(int originX) {
		this.originX = originX;
	}

	public void setOriginY(int originY) {
		this.originY = originY;
	}
	
	public void setSuper(boolean superCharacter){
		this.superCharacter = superCharacter;
	}

	public void setJumped(boolean jumped) {
		this.jumped = jumped;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public boolean isDucked() {
		return ducked;
	}

	public void setDucked(boolean ducked) {
		this.ducked = ducked;
	}

	public boolean isMovingRight() {
		return movingRight;
	}

	public void setMovingRight(boolean movingRight) {
		this.movingRight = movingRight;
	}

	public boolean isMovingLeft() {
		return movingLeft;
	}

	public void setMovingLeft(boolean movingLeft) {
		this.movingLeft = movingLeft;
	}
	
	public ArrayList getFireballs() {
		return fireballs;
	}

}
package koji;

public class Enemy {
	
	private int maxHealth, currentHealth, speedX, originX, originY, pathForward = 0, pathBackward = 0;
	private Background background = StartGame.getBg1();
	

	public void update() {

		originX += speedX; //Keeps the goomba in it's place while the player moves.
		speedX = background.getSpeedX();
		patrolPath();
		
	}
	
	
	public void attack() {
		
	}
	
	public void death() {

		
	}
	
	public void patrolPath(){
		if(pathForward <= 20){
			originX++;
			pathForward++;
	}
		else if(pathForward > 20 && pathForward <= 40){
			originX--;
			pathForward++;
		}
		else{
			pathForward = 0;
		}
		
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
	}

	public int getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = currentHealth;
	}


	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getOriginX() {
		return originX;
	}

	public void setOriginX(int originX) {
		this.originX = originX;
	}

	public int getOriginY() {
		return originY;
	}

	public void setOriginY(int originY) {
		this.originY = originY;
	}

	public Background getBg() {
		return background;
	}

	public void setBg(Background background) {
		this.background = background;
	}

}

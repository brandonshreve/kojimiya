package koji;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import java.util.ArrayList;

public class StartGame extends Applet implements Runnable, KeyListener {
	
	private playerCharacter player; 
    private Goomba goomba1;
    private Clouds cloud;
    private Image image, currentSprite, character, 
            characterDown, characterJumped, background, enemyGoomba; 
    private Graphics second; 
    private URL base; 
    private static Background bg1, bg2; 
    private Animation charAnim, goombaAnim; 
    private Image[] walkingAnimation = new Image[9];
    private Image[] cloudSequence = new Image[3];
    private int walkFrame = 0;
      

  
    @Override
    public void init() { 
  
    	Frame f = (Frame) this.getParent().getParent(); 
        f.setTitle("Kojimiya Game");
        setSize(600, 385); 
        setBackground(Color.WHITE); 
        setFocusable(true); 
        addKeyListener(this); 
        
        
        try { 
            base = getDocumentBase(); 
        } catch (Exception e) { 
        	System.err.println("Exception: " + e.getMessage()); 
        } 
  
        // Image Setups 
        character = getImage(base, "marioAssets/Mario.gif"); 
        characterDown = getImage(base, "marioAssets/SuperMarioDuck.gif"); 
        characterJumped = getImage(base, "marioAssets/MarioJump.gif"); 

        enemyGoomba = getImage(base, "enemyAssets/Goomba.gif"); 
        background = getImage(base, "environmentAssets/World1-1.png"); 
     
        walkingAnimation[0] = getImage(base, "marioAssets/MarioWalk1.gif");
        walkingAnimation[1] = getImage(base, "marioAssets/MarioWalk1.gif");
        walkingAnimation[2] = getImage(base, "marioAssets/MarioWalk1.gif");
        walkingAnimation[3] = getImage(base, "marioAssets/MarioWalk2.gif");
        walkingAnimation[4] = getImage(base, "marioAssets/MarioWalk2.gif");
        walkingAnimation[5] = getImage(base, "marioAssets/MarioWalk2.gif");
        walkingAnimation[6] = getImage(base, "marioAssets/MarioWalk3.gif");
        walkingAnimation[7] = getImage(base, "marioAssets/MarioWalk3.gif");
        walkingAnimation[8] = getImage(base, "marioAssets/MarioWalk3.gif");
        
        cloudSequence[0] = getImage(base, "environmentAssets/cloudSingle.gif");
        cloudSequence[1] = getImage(base, "environmentAssets/cloudDouble.gif");
        cloudSequence[2] = getImage(base, "environmentAssets/cloudTriple.gif");

        
        charAnim = new Animation(); 
        charAnim.addFrame(character, 1250); 

        goombaAnim = new Animation(); 
        goombaAnim.addFrame(enemyGoomba, 100); 

        currentSprite = charAnim.getImage();
    } 
  
    
    
    @Override
    public void start() { 
    	
        bg1 = new Background(0, 0); 
        bg2 = new Background(600, 0); 
        cloud = new Clouds();
        
        player = new playerCharacter();
        goomba1 = new Goomba(300, 340); 
        
  
        Thread thread = new Thread(this); 
        thread.start(); 
        
    } 
  

    @Override
    public void stop() { 

    } 
  
    @Override
    public void destroy() { 

    } 
  
    public void run() { 
        while (true) { 
        	
            player.update(); 
            
            if (player.isJumped()) { 
                currentSprite = characterJumped; 
            } else if (player.isJumped() == false && player.isDucked() == false) { 
                currentSprite = charAnim.getImage(); 
            }
            
            //Attempting to smoothly animate running when player isMovingRight
            if ((player.isMovingRight() || player.isMovingLeft()) && player.isDucked() == false && player.isJumped() == false){
            	
            	if(walkFrame < 9) {
            	currentSprite = walkingAnimation[walkFrame];
            	walkFrame++;
            	}
            
            else {
            	walkFrame = 0;
            	currentSprite = walkingAnimation[walkFrame];
            	walkFrame++;
            	//System.out.println("Hello " + walkFrame + "   " + currentSprite);
            }
            	
           }
            
  
            ArrayList fireballs = player.getFireballs(); 
            
            for (int i = 0; i < fireballs.size(); i++) { 
                Fireball f = (Fireball) fireballs.get(i); 
                if (f.isVisible() == true) { 
                    f.update(); 
                } else { 
                    fireballs.remove(i); 
                } 
            } 
  
            goomba1.update();  
            cloud.update();
            bg1.update(); 
            bg2.update();  
            repaint(); 
            try { 
                Thread.sleep(17); 
            } catch (InterruptedException e) { 
                e.printStackTrace(); 
            } 
        } 
   } 
  
  
    @Override
    public void update(Graphics g) { 
        if (image == null) { 
            image = createImage(this.getWidth(), this.getHeight()); 
            second = image.getGraphics(); 
        } 
  
        second.setColor(getBackground()); 
        second.fillRect(0, 0, getWidth(), getHeight()); 
        second.setColor(getForeground()); 
        paint(second); 
  
        g.drawImage(image, 0, 0, null); 
  
    } 
  
  //Painting the images onto the Applet. Study the paint() method more!
    @Override
    public void paint(Graphics g) { 
    	
    	//drawImage(image, xCoord, yCoord, observer). observer set to null. Read up on observer Object.
        g.drawImage(background, bg1.getBgX(), bg1.getBgY(), null); 
        g.drawImage(background, bg2.getBgX(), bg2.getBgY(), null); 
        g.drawImage(currentSprite, (player.getOriginX() - 61), (player.getOriginY() - 63), null); 
        g.drawImage(goombaAnim.getImage(), (goomba1.getOriginX() - 48), (goomba1.getOriginY() - 48), null); 
        
        //Update this to act more like the Fireballs
        g.drawImage(cloudSequence[0], (cloud.getOriginX()), (cloud.getOriginY() - 50), null);
        g.drawImage(cloudSequence[1], (cloud.getOriginX() + 200), (cloud.getOriginY() + 20), null);
        g.drawImage(cloudSequence[2], (cloud.getOriginX() - 200), (cloud.getOriginY()), null);
        g.drawImage(cloudSequence[0], (cloud.getOriginX() + 600), (cloud.getOriginY() - 50), null);
        g.drawImage(cloudSequence[1], (cloud.getOriginX() + (200 + 600)), (cloud.getOriginY() + 20), null);
        g.drawImage(cloudSequence[2], (cloud.getOriginX() + 400), (cloud.getOriginY()), null);
        
        ArrayList fireballs = player.getFireballs(); 
        for (int i = 0; i < fireballs.size(); i++) { 
            Fireball f = (Fireball) fireballs.get(i); 

            g.setColor(Color.ORANGE); 
            g.fillRect(f.getX(), f.getY(), 7, 6); 
        } 
    } 
  
    @Override
    public void keyPressed(KeyEvent e) { 
  
        switch (e.getKeyCode()) { 
        
        case KeyEvent.VK_UP: 
        	player.jump(); 
            break; 
  
        case KeyEvent.VK_DOWN: 
        	if (player.isSuper() == true){
            currentSprite = characterDown; 
        	}
        	
            if (player.isJumped() == false) { 
                player.setDucked(true); 
                player.setSpeedX(0); 
            } 
            break; 
              
        case KeyEvent.VK_S: 
        	if (player.isSuper() == true){
                currentSprite = characterDown; 
        	}

            if (player.isJumped() == false) { 
                player.setDucked(true); 
                player.setSpeedX(0); 
            } 
            break; 
  
        case KeyEvent.VK_LEFT: 
            player.moveLeft(); 
            player.setMovingLeft(true); 
            break; 
            
        case KeyEvent.VK_W: 
            player.jump(); 
            break; 
              
        case KeyEvent.VK_A: 
            player.moveLeft(); 
            player.setMovingLeft(true); 
            break; 
  
        case KeyEvent.VK_RIGHT: 
            player.moveRight(); 
            player.setMovingRight(true); 
            break; 
              
        case KeyEvent.VK_D: 
            player.moveRight(); 
            player.setMovingRight(true); 
            break; 
  
        case KeyEvent.VK_SPACE: 
            player.jump(); 
            break; 
  
        case KeyEvent.VK_CONTROL: 
            if (player.isDucked() == false){
                player.shoot(); 
                player.fireballCounter(); 
            } 
            break; 
  
        } 
  
    } 
  
    @Override
    public void keyReleased(KeyEvent e) { 
        switch (e.getKeyCode()) { 
          
        case KeyEvent.VK_UP: 
            player.jumpCounter(); 
            break; 
              
        case KeyEvent.VK_W: 
            player.jumpCounter(); 
 
            break; 
              
        case KeyEvent.VK_DOWN: 
            currentSprite = charAnim.getImage(); 
            player.setDucked(false); 
            break; 
              
        case KeyEvent.VK_S: 
        	currentSprite = charAnim.getImage(); 
            player.setDucked(false); 
            break; 
  
        case KeyEvent.VK_LEFT: 
            player.stopLeft(); 
            break; 
              
        case KeyEvent.VK_A: 
            player.stopLeft(); 
            break; 
  
        case KeyEvent.VK_RIGHT: 
            player.stopRight(); 
            break; 
              
        case KeyEvent.VK_D: 
            player.stopRight(); 
            break; 
  
        case KeyEvent.VK_SPACE: 
            player.jumpCounter(); 
            break; 
  
        } 
  
    } 
  
    @Override
    public void keyTyped(KeyEvent e) { 
        // TODO Auto-generated method stub 
    } 
  
    public static Background getBg1() { 
        return bg1; 
    } 
  
    public static Background getBg2() { 
        return bg2; 
    } 
      

 

}

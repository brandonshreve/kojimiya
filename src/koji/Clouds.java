package koji;

import java.util.Random;

public class Clouds {
	

	private Random generator = new Random();
	private int originX, originY, tempCloud;
	private Background background = StartGame.getBg1();

	
	public void update() {
		originY = 100;
		originX += (1 + background.getSpeedX());
	}
	
	public int randomCloud(){
		this.setTempCloud(generator.nextInt(3));
		return this.getTempCloud();
	}

	public int getTempCloud() {
		return tempCloud;
	}

	
	public int getOriginX(){
		return originX;
	}
	
	
	public int getOriginY(){
		return originY;
	}
	
	public void setTempCloud(int tempCloud) {
		this.tempCloud = tempCloud;
	}
	
	public void setOriginX(int originX) {
		this.originX = originX;
	}
	
	public void setOriginY(int originY) {
		this.originY = originY;
	}
	
	



	
	
	
}
